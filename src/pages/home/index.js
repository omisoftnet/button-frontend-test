import React from 'react';

import settingIcon from '../../assets/setting.svg';
import { Button } from '../../components/button';

import styles from  './Home.module.css';


export default () => {
    return (
        <div className={styles.wrapper}>
            <Button
                type="contain"
                color="primary"
            >
                contain
            </Button>
            <br/>
            <br/>
            <Button
                type="outline"
                color="secondary"
            >
                outline
            </Button>
            <br/>
            <br/>
            <Button
                classes={[styles.customButton]}
                type="contain"
                color="primary"
            >
                Custom Settin
                {/* i think the best way add icons when user have total influence on this process */}
                <img src={settingIcon} alt="settingIcon"/>
            </Button>
        </div>
    )
}
