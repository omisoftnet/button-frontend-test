import React from 'react';

import styles from './Button.module.css';

export const Button = ({ children, color, type, classes }) => {
    // if we will use jss will be better do theme and customize all this thing seperately
    const palitra = {
        primary: styles.primaryColor,
        secondary: styles.secondaryColor,
    }

    const typeButton = {
        outline: styles.outlineButton,
        contain: styles.containButton
    }

    return (
        <button className={`
            ${classes ? classes.reduce((cur, prev) => `${prev} ${cur}`, '') : ''}
            ${styles.basicButton}
            ${palitra[color]}
            ${typeButton[type]}
            `}>
            { children }
        </button>
    );
}